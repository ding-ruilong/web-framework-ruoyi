import java.util.HashMap;
import java.util.Map;

public class TopKeywordsTracker {
    private Map<String, Integer> keywordCounts = new HashMap<>();

    public void track(String keyword) {
        // 记录关键词出现次数
        if (keywordCounts.containsKey(keyword)) {
            keywordCounts.put(keyword, keywordCounts.get(keyword) + 1);
        } else {
            keywordCounts.put(keyword, 1);
        }
    }

    public String[] getTopKeywords() {
        // 获取每日搜索最高的五个关键词
        return keywordCounts.entrySet().stream()
                .sorted(Map.Entry.<String, Integer>comparingByValue().reversed())
                .limit(5)
                .map(Map.Entry::getKey)
                .toArray(String[]::new);
    }
}

