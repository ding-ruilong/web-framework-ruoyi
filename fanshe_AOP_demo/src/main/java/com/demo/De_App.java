public class De_App {
    public static void main(String[] args) {
        // 使用反射创建就业搜索 对象
        EmploymentSearch employmentSearch = createEmploymentSearch();
        // 用户输入的就业关键词
        String keyword = "JAVA";
        System.out.println("用户搜索内容：" + keyword+"\n");
        // 进行搜索
        String searchResult = employmentSearch.search(keyword);
        System.out.println("搜索结果：" + searchResult + "\n");
        // 创建 TopKeywordsTracker 的实例
        TopKeywordsTracker tracker = new TopKeywordsTracker();
        // 模拟记录搜索关键词
        String key_top = "JAVA, Python, 爬虫, 大数据, HTML";
        tracker.track(key_top);

        // 获取每日搜索最高的五个关键词
        String[] topKeywords = tracker.getTopKeywords();
        System.out.println("每日搜索最高的五个关键词：");
        for (String topKeyword : topKeywords) {
            System.out.println(topKeyword);
        }
    }
    // 使用反射创建就业搜索对象
    private static EmploymentSearch createEmploymentSearch() {
        try {
            Class<?> clazz = Class.forName("WeiboEmploymentSearch");
            return (EmploymentSearch) clazz.getDeclaredConstructor().newInstance();
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }
}
