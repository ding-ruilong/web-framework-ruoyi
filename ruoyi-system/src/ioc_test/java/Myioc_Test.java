package src.ioc_test.java;
import com.ruoyi.system.Ioc.dao.UserDaoMysqlImpl;
import com.ruoyi.system.Ioc.service.UserService;
import com.ruoyi.system.Ioc.service.UserServiceImpl;

public class Myioc_Test {
    public static void main(String[] args) {
        UserService userService = new UserServiceImpl();
        ((UserServiceImpl) userService).setUserDaoAfter(new UserDaoMysqlImpl());
        userService.getUser();
    }
}
