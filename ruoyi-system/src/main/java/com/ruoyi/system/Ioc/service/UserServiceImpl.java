package com.ruoyi.system.Ioc.service;
import com.ruoyi.system.Ioc.dao.UserDao;

public class UserServiceImpl implements UserService{
    private UserDao userDaoAfter;

    public void setUserDaoAfter(UserDao userDaoAfter) {
        this.userDaoAfter = userDaoAfter;
    }
    public void getUser() {
        userDaoAfter.getUser();
    }

}
